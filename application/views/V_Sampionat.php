<html>
    <head>
        <script src="<?php echo base_url(); ?>data/js/scripts.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>data/js/owl-carousel.js" type="text/javascript"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    </head>
    <body>
        <header class="header">
            <h1 class="screenReaderElement">Rally</h1>
            <div class="container-half">
                <div class="popis">
                    <div class="text">
                        <p>Projekt vytvořen na základě školního projektu v předmětu DAS</p>
                    </div>
                </div>
            </div>
        </header>
        <nav role="navigation">
            <div class="container-half">
                <a href="<?php echo base_url(); ?>" class="img"><img src="<?php echo base_url('data/images/logo.png'); ?>"></a>
                <ul>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                </ul>
                <a class="insert">Insert <i class="fas fa-pencil-alt"></i></a>
                <a class="update">Update <i class="fas fa-wrench"></i></a>
            </div>
        </nav>
        <section class="race">
            <div class="container-half">
                <?php
                //echo heading("Stanice v zemi");
                foreach ($data as $row) {
                    $atributy = array(
                        "class" => "btn"
                    );
                    $pole = array(
                        "Nazev závodu: " . $row->nazevZavodu,
                        "Datum závodu: " . $row->datumZavodu,
                        "Nazev města: " . $row->mesto,
                        "Délka závodu: " . $row->delkaZavodu . " Km",
                        "<img src =" . base_url() . "/data/images/vlajky/" . $row->vlajka . ">",
                        anchor("zavod/" . $row->idzavod, "Přejdi na závod", $atributy),
                        anchor("update/" . $row->idzavod, "Update dat", $atributy),
                        anchor("delete/zavod/".$row->idzavod, "Vymazat zavod", $atributy)
                    );
                    ?>
                    <li><div class="champ">

                            <p> <?php echo $pole[0]; ?></p><br/>
                            <p> <?php echo $pole[1]; ?></p><br/>
                            <p> <?php echo $pole[2]; ?></p><br/>
                            <p> <?php echo $pole[3]; ?></p><br/>
                            <p> <?php echo $pole[4]; ?></p><br/>
                            <p id="left"> <?php echo $pole[5] ?></p><br/>
                            <p id="right"> <?php echo $pole[6] ?></p>
                            <p id="right"> <?php echo $pole[7] ?></p>
<?php };
?>
                    </div></li>
            </div>
        </section>
    </body>
</html>
<html>
    <head>
        <script src="<?php echo base_url(); ?>data/js/scripts.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>data/js/owl-carousel.js" type="text/javascript"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    </head>
    <body>
        <header class="header">
            <h1 class="screenReaderElement">Rally</h1>
            <div class="container-half">
                <div class="popis">
                    <div class="text">
                        <p>Projekt vytvořen na základě školního projektu v předmětu DAS</p>
                    </div>
                </div>
            </div>
        </header>
        <nav role="navigation">
            <div class="container-half">
                <a href="" class="img"><img src="<?php echo base_url('data/images/logo.png'); ?>"></a>
                <ul>
                    <li><a href="../vyrobci">Výrobci</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                </ul>
                <a class="insert">Insert <i class="fas fa-pencil-alt"></i></a>
                <a class="update">Update <i class="fas fa-wrench"></i></a>
            </div>
        </nav>
        <section class="car">
            <div class="container-half">

                <?php
                foreach ($data as $row) {
                    $pole = array(
                        "<strong>Model auta:</strong> " . $row->model,
                        "<strong>Rok výroby:</strong> " . $row->rokVyroby,
                        "<strong>Typ auta:</strong> " . $row->nazev,
                        "<strong>Obsah:</strong> " . $row->obsahLimit,
                        "<strong>Výrobce:</strong> " . $row->nazevVyrobce,
                        "<img id='vlajka' src =" . base_url() . "/data/images/vlajky/" . $row->vlajka . ">",
                        "<img id='brand' src =" . base_url() . "/data/images/znacky/" . $row->logo . ">",
                    );
                    ?>
                    <center><div class="champi">

                            <div class="car-info">
                                <p> <?php echo $pole[0]; ?></p><br/>
                                <p> <?php echo $pole[1]; ?></p><br/>
                                <p> <?php echo $pole[2]; ?></p><br/>
                                <p> <?php echo $pole[3]; ?></p><br/>
                            </div>
                            <div class="creator-info">
                                <p> <?php echo $pole[4]; ?></p><br/>
                                <div class="world">
                                    <p> <?php echo $pole[5]; ?></p>
                                </div>
                                <div class="brand">
                                    <p> <?php echo $pole[6]; ?></p>
                                </div>
                            </div>
                           
                        <?php }
                        ?>

                    </div></center>

            </div>
        </section>
    </body>
</html>
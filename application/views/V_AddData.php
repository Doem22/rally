<html>
    <head>
        <script src="<?php echo base_url(); ?>data/js/scripts.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>data/js/owl-carousel.js" type="text/javascript"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    </head>
    <body>
        <header class="header">
            <h1 class="screenReaderElement">Rally</h1>
            <div class="container-half">
                <div class="popis">
                    <div class="text">
                        <p>Projekt vytvořen na základě školního projektu v předmětu DAS</p>
                    </div>
                </div>
            </div>
        </header>
        <nav role="navigation">
            <div class="container-half">
                <a href="<?php echo base_url(); ?>" class="img"><img src="<?php echo base_url('data/images/logo.png'); ?>"></a>
                <ul>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                </ul>
                <a class="insert">Insert <i class="fas fa-pencil-alt"></i></a>
                <a class="update">Update <i class="fas fa-wrench"></i></a>
            </div>
        </nav>
        <section class="adddata">
            <div class="container-half">
                <center><div class="racers">
                        <div class="champ">
                            <img src="<?php echo base_url();?>data/images/loeb.png">
                            <?php
                            $attributes = array(
                                'class' => 'form-horizontal'
                            );

                            echo form_open('pridat/vyrobce', $attributes);

                            echo "<div class='form-input'>";


                            $attributes = array(
                                'class' => 'input',
                                'id' => 'nazevvyr',
                                'placeholder' => 'Název výrobce',
                                'name' => 'nazevvyr'
                            );
                            echo form_input($attributes);
                            echo '</div>';

                            echo "<div class='form-input'>";

                            $attributes = array(
                                'class' => 'input',
                                'id' => 'logo',
                                'placeholder' => 'Logo',
                                'name' => 'logo'
                            );
                            echo form_input($attributes);
                            echo '</div>';

                            echo "<div class='form-input'>";

                            foreach ($zeme as $row) {
                                $options3[$row->idzeme] = $row->nazev;
                            }
                            $settings = array(
                                'class' => 'input'
                            );

                            echo form_dropdown('idzeme', $options3, '', $settings);

                            echo "</div>";


                            $data = array(
                                'name' => 'pridat',
                                'type' => 'submit',
                                'content' => 'Přidat záznam',
                                'class' => 'btn btn-default'
                            );
                            echo "<div class='form-group'>";
                            echo form_button($data);
                            echo "</div>";
                            echo form_close();
                            ?>
                        </div>
                    </div></center>
                <center> <div class="zavody">
                        <div class="champ">
                            <img src="<?php echo base_url();?>data/images/car1.png">
                            <?php
                            echo form_open('pridat/zavod', $attributes);

                            echo "<div class='form-input'>";


                            $attributes = array(
                                'class' => 'input',
                                'id' => 'nazevzav',
                                'placeholder' => 'Název závodu',
                                'name' => 'nazevzav'
                            );
                            echo form_input($attributes);
                            echo '</div>';
                            //radek
                            echo "<div class='form-input'>";


                            $attributes = array(
                                'class' => 'input',
                                'id' => 'datum',
                                'placeholder' => 'Formát : 2000-01-01',
                                'name' => 'datum'
                            );
                            echo form_input($attributes);
                            echo '</div>';
                            //radek
                            echo "<div class='form-input'>";

                            foreach ($zeme as $row) {
                                $options3[$row->idzeme] = $row->nazev;
                            }
                            $settings = array(
                                'class' => 'input'
                            );

                            echo form_dropdown('idzemeZav', $options3, '', $settings);
                            echo "</div>"; //</radek>
                            //radek               
                            echo "<div class='form-input'>";

                            $attributes = array(
                                'class' => 'input',
                                'id' => 'mesto',
                                'placeholder' => 'Město uskutečnění',
                                'name' => 'mesto'
                            );
                            echo form_input($attributes);
                            echo '</div>';
                            //radek
                            echo "<div class='form-input'>";

                            $attributes = array(
                                'class' => 'input',
                                'id' => 'delka',
                                'placeholder' => 'Délka závodu',
                                'name' => 'delka'
                            );
                            echo form_input($attributes);
                            echo '</div>';
                            //radek
                            $data = array(
                                'name' => 'pridat',
                                'type' => 'submit',
                                'content' => 'Přidat záznam',
                                'class' => 'btn btn-default'
                            );

                            echo form_button($data);

                            echo form_close();
                            ?>
                        </div>
                    </div></center>s
            </div>
        </section>
    </body>
</html>
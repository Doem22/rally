
<html>
    <head>
        <script src="<?php echo base_url(); ?>data/js/scripts.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>data/js/owl-carousel.js" type="text/javascript"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    </head>
    <body>
        <header class="header">
            <h1 class="screenReaderElement">Rally</h1>
            <div class="container-half">
                <div class="popis">
                    <div class="text">
                        <p>Projekt vytvořen na základě školního projektu v předmětu DAS</p>
                    </div>
                </div>
            </div>
        </header>
        <nav role="navigation">
            <div class="container-half">
                <a href="#" class="img"><img src="<?php echo base_url('data/images/logo.png'); ?>"></a>
                <ul>
                    <li><a href="vyrobci">Výrobci</a></li>
                    <li><a href="zavodnici">Závodnici</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                </ul>
                <a class="insert" href="pridat">Insert <i class="fas fa-pencil-alt"></i></a>
                <a class="update" href="">Update <i class="fas fa-wrench"></i></a>
            </div>
        </nav>
        <main role="main">
            <section class="showcase">
                <div class="container-half" id="text">
                    <div class="left">
                        <h2>Projekt Rally</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus luctus egestas leo. Sed ac dolor sit amet purus malesuada congue. Nulla est. Nunc auctor. Aenean vel massa quis mauris vehicula lacinia.</p>
                        <p> Phasellus et lorem id felis nonummy placerat. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Nullam eget nisl. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</p>
                    </div>
                    <div class="right">
                        <img>
                    </div>
                </div>
            </section>
            <section class="information">
                <div class="container-half">
                    <div class="describe">
                        <h2>Informace</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus luctus egestas leo. Sed ac dolor sit amet purus malesuada congue. Nulla est. Nunc auctor. Aenean vel massa quis mauris vehicula lacinia. Phasellus et lorem id felis nonummy placerat. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Nullam eget nisl. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</p>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus luctus egestas leo. Sed ac dolor sit amet purus malesuada congue. Nulla est. Nunc auctor. Aenean vel massa quis mauris vehicula lacinia. Phasellus et lorem id felis nonummy placerat. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Nullam eget nisl. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.</p>
                    </div>
                    <div class="image">
                        <center><img src="<?php echo base_url(); ?>data/images/car4.png"></center>
                    </div>

                </div>
            </section>
            <section class="champions">
                <div class="container-half">
                    <h1>Šampionát</h1>
                    <h2></h2>
                    <ul>
                        <?php
                        $atributy = array(
                            "style" => "text-decoration: none; font-family: 'Anton', sans-serif;"
                        );
                        foreach ($data as $row) {
                            $pole = array(
                                anchor("sampionaty/" . $row->idsampionat, $row->idsampionat . ". " . $row->nazev, $atributy),
                                
                                
                            );
                            
                            ?>
                            <li>
                                <div class="champ">
                                    <p> <?php echo $pole[0]; ?></p>

                                </div>
                            </li> <?php
                    }
                        ?>

                    </ul>
                </div>
            </section>
            <section class="footer">
                <div class="container-half">
                    <div class="col-1">

                    </div>
                    <div class="col-1">

                    </div>

                </div>
            </section>
        </main>
    </body>
</html>

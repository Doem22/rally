<html>
    <head>
        <script src="<?php echo base_url(); ?>data/js/scripts.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>data/js/owl-carousel.js" type="text/javascript"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    </head>
    <body>
        <header class="header">
            <h1 class="screenReaderElement">Rally</h1>
            <div class="container-half">
                <div class="popis">
                    <div class="text">
                        <p>Projekt vytvořen na základě školního projektu v předmětu DAS</p>
                    </div>
                </div>
            </div>
        </header>
        <nav role="navigation">
            <div class="container-half">
                <a href="#" class="img"><img src="<?php echo base_url('data/images/logo.png'); ?>"></a>
                <ul>
                    <li><a href="vyrobci">Výrobci</a></li>
                    <li><a href="#">Závodnici</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                </ul>
                <a class="insert" href="pridat">Insert <i class="fas fa-pencil-alt"></i></a>
                <a class="update" href="">Update <i class="fas fa-wrench"></i></a>
            </div>
        </nav>
        <section class="racerss">
            <div class="container-half">
                <ul>
                    <?php
                    foreach ($zavodnici as $row) {
                        $atributy = array(
                            'class' => 'btn'
                        );
                        $array = array(
                            "Jméno a příjmeni: " . $row->jmeno . " " . $row->prijmeni,
                            "Rok zarození: " . $row->rokNarozeni,
                            "Stát původu: " . $row->nazev,
                            "<img src =" . base_url() . "/data/images/vlajky/" . $row->vlajka . ">",
                            anchor("zavodnici/edit/" . $row->idzavodnik, "Update dat", $atributy),
                            anchor("delete/zavodnik/".$row->idzavodnik, "Vymazat data", $atributy),
                            anchor("obnovit/zavodnik/")
                        );
                        ?>
                        <li> <div class="champ">
                                <p> <?php echo $array[0]; ?></p><br/>
                                <p> <?php echo $array[1]; ?></p><br/>
                                <p> <?php echo $array[2]; ?></p><br/>
                                <p> <?php echo $array[3]; ?></p><br/>
                                <div class="left">
                                    <p> <?php echo $array[4]; ?></p><br/>
                                </div>
                                <div class="right">
                                    <p> <?php echo $array[5]; ?></p>
                                </div>
                               
                            </div></li>
                        <?php
                    }
                    ?>
                </ul>

            </div>
        </section>
    </body>
</html>
<html>
    <head>
        <script src="<?php echo base_url(); ?>data/js/scripts.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>data/js/owl-carousel.js" type="text/javascript"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    </head>
    <body>
        <header class="header">
            <h1 class="screenReaderElement">Rally</h1>
            <div class="container-half">
                <div class="popis">
                    <div class="text">
                        <p>Projekt vytvořen na základě školního projektu v předmětu DAS</p>
                    </div>
                </div>
            </div>
        </header>
        <nav role="navigation">
            <div class="container-half">
                <a href="<?php echo base_url(); ?>" class="img"><img src="<?php echo base_url('data/images/logo.png'); ?>"></a>
                <ul>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                    <li><a>blabla</a></li>
                </ul>
                <a class="insert">Insert <i class="fas fa-pencil-alt"></i></a>
                <a class="update">Update <i class="fas fa-wrench"></i></a>
            </div>
        </nav>
        <section class="updated">
            <div class="container-half">
                <center> <div class="champ">
                        <?php
                        
                        echo form_open("success/".$data[0]->idzavod);


                        $atributy = array(
                            'class' => 'input',
                            'id' => 'nazev',
                            'value' => $data[0]->nazevZavodu,
                            'name' => 'nazev'
                        );
                        echo "<div class='form-input'>";
                        echo form_input($atributy);
                        echo "</div>";

                        $atributy = array(
                            'id' => 'datum',
                            'class' => 'input',
                            'value' => $data[0]->datumZavodu,
                            'name' => 'datum'
                        );
                        echo "<div class='form-input'>";
                        echo form_input($atributy);
                        echo "</div>";

                        $atributy = array(
                            'id' => 'mesto',
                            'class' => 'input',
                            'value' => $data[0]->mesto,
                            'name' => 'mesto'
                        );
                        echo "<div class='form-input'>";
                        echo form_input($atributy);
                        echo "</div>";

                        $atributy = array(
                            'id' => 'delka',
                            'class' => 'input',
                            'value' => $data[0]->delkaZavodu,
                            'name' => 'delka'
                        );
                        echo "<div class='form-input'>";
                        echo form_input($atributy);
                        echo "</div>";
                        
                        foreach($drop as $row){
                            $option[$row->idzeme] = $row->nazev;
                        }
                        $atributy = array(
                            'class'=>'input'
                        );
                        echo "<div class='form-input'>";
                        echo form_dropdown("drop",$option,"",$atributy);
                        echo "</div>";
                        $atributy = array(
                            'id' => 'button',
                            'class' => 'btn btn-default',
                            'type' => 'submit',
                            'content' => 'Aktualizovat'
                        );
                        
                        echo form_button($atributy);
                        
                        echo form_close();
                        ?>
                    </div></center>
            </div>
        </section>
    </body>
</html>
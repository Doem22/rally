<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mode
 *
 * @author rihak_dominik
 */
class Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function uvod() {
        $this->db->select('idsampionat, nazev');
        $this->db->from('sampionat');
        $this->db->order_by('idsampionat', 'ASC');
        $data = $this->db->get()->result();
        return $data;
    }

    public function sampionat($idSampionat) {

        $this->db->select('zavod.idzavod,zavod.nazevZavodu, zavod.datumZavodu, zavod.zeme, zavod.mesto, zavod.delkaZavodu, zeme.vlajka');
        $this->db->from('zavod');
        $this->db->join('sampionat_has_zavod', '.zavod.idzavod = sampionat_has_zavod.zavod_idzavod', 'inner');
        $this->db->join('zeme', 'zavod.zeme = zeme.idzeme', 'inner');
        $this->db->where('sampionat_idsampionat', $idSampionat);
        $this->db->where('delete',0);
        $data = $this->db->get()->result();
        return $data;
    }

    public function getZavodInfo($id) {
        $this->db->select('automobil.idautomobil,vysledek.idvysledek,vysledek.poradi, vysledek.cas, vysledek.ridic, vysledek.spolujezdec, vysledek.automobil, vysledek.tym, tym.nazevTymu, zavodnik.jmeno, zavodnik.prijmeni, zavodnik.zeme,spolujezdec.jmeno as jmeno_spolujezdec, spolujezdec.prijmeni as prijmeni_spolujezdec');
        $this->db->from('vysledek');
        $this->db->join('zavodnik', 'vysledek.ridic=zavodnik.idzavodnik', 'inner');
        $this->db->join('zavodnik as spolujezdec', 'vysledek.spolujezdec = spolujezdec.idzavodnik', 'inner');
        $this->db->join('tym', 'vysledek.tym=tym.idtym', 'inner');
        $this->db->join('automobil', 'vysledek.automobil = automobil.idautomobil', 'inner');
        $this->db->where('vysledek.zavod', $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function getAuto($id) {
        $this->db->select('automobil.model, automobil.rokVyroby, automobil.typAuta, automobil.vyrobce, typauta.obsahLimit, typauta.nazev, vyrobce.nazevVyrobce, vyrobce.logo, vyrobce.zeme, zeme.vlajka');
        $this->db->from('automobil');
        $this->db->join('typauta', 'automobil.typAuta = typauta.idtypAuta', 'inner');
        $this->db->join('vyrobce', 'automobil.vyrobce = vyrobce.idvyrobce');
        $this->db->join('zeme', 'vyrobce.zeme = zeme.idzeme', 'inner');
        $this->db->where('automobil.idautomobil', $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function getVyrobce() {
        $this->db->select('vyrobce.idvyrobce,vyrobce.logo,vyrobce.nazevVyrobce,vyrobce.zeme');
        $this->db->from('vyrobce');
        $this->db->join('zeme', ' vyrobce.zeme = zeme.idzeme', 'inner');
        $this->db->order_by('vyrobce.nazevVyrobce', 'ASC');
        $data = $this->db->get()->result();
        return $data;
    }

    public function getVyrobceAuto($id) {
        $this->db->select('automobil.idautomobil, automobil.model, automobil.rokVyroby, automobil.typAuta,automobil.vyrobce, vyrobce.nazevVyrobce,typauta.nazev');
        $this->db->from('automobil');
        $this->db->join('vyrobce', 'automobil.vyrobce = vyrobce.idvyrobce', 'inner');
        $this->db->join('typAuta', 'automobil.typauta = typAuta.idtypAuta', 'inner');
        $this->db->where('vyrobce.idvyrobce', $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function getZavodnici($preskocit = 0, $pocet = 0) {
        $this->db->select('idzavodnik, jmeno, prijmeni,rokNarozeni,zeme.nazev,zeme.vlajka');
        $this->db->from('zavodnik');
        $this->db->limit($pocet, $preskocit);
        $this->db->join('zeme', 'zavodnik.zeme = zeme.idzeme', 'inner');
        $this->db->order_by('idzavodnik', 'ASC');
        $this->db->where('smaz',0);
        $data = $this->db->get()->result();
        return $data;
    }

    public function zeme() {
        $this->db->select('idzeme ,nazev');
        $this->db->from('zeme');
        $this->db->order_by('nazev', 'ASC');
        $data = $this->db->get()->result();
        return $data;
    }

    public function addVyrobce($nazevVyrobce, $logo, $zeme) {
        $data = array(
            'nazevVyrobce' => $nazevVyrobce,
            'logo' => $logo,
            'zeme' => $zeme
        );
        $this->db->insert('vyrobce', $data);
    }

    public function addZavod($nazevZavodu, $datemZavodu, $zeme, $mesto, $delkaZavodu) {
        $data = array(
            'nazevZavodu' => $nazevZavodu,
            'datumZavodu' => $datemZavodu,
            'zeme' => $zeme,
            'mesto' => $mesto,
            'delkaZavodu' => $delkaZavodu
        );
        $this->db->insert('zavod', $data);
    }

    public function getEditVyrobce($id) {
        $this->db->select('vyrobce.idvyrobce,vyrobce.logo,vyrobce.nazevVyrobce,vyrobce.zeme');
        $this->db->from('vyrobce');
        $this->db->join('zeme', ' vyrobce.zeme = zeme.idzeme', 'inner');
        $this->db->order_by('vyrobce.nazevVyrobce', 'ASC');
        $this->db->where('vyrobce.idvyrobce', $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function smazatZavod($id) {
        $data = array('delete' => 1);
        $this->db->where('idzavod', $id);
        $this->db->update('zavod', $data);
    }

    public function smazatZavodnik($id) {
        $data = array('smaz' => 1);
        $this->db->where('idzavodnik', $id);
        $this->db->update('zavodnik', $data);
    }

    public function getInfo($id) {
        $this->db->select('zavod.idzavod,zavod.nazevZavodu, zavod.datumZavodu, zavod.zeme, zavod.mesto, zavod.delkaZavodu, zeme.vlajka');
        $this->db->from('zavod');
        $this->db->join('sampionat_has_zavod', '.zavod.idzavod = sampionat_has_zavod.zavod_idzavod', 'inner');
        $this->db->join('zeme', 'zavod.zeme = zeme.idzeme', 'inner');
        $this->db->where('idzavod', $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function setInfo($nazev, $datum, $zeme, $mesto, $delkazavodu, $id) {
        $data = array(
            'nazevZavodu' => $nazev,
            'datumZavodu' => $datum,
            'zeme' => $zeme,
            'mesto' => $mesto,
            'delkaZavodu' => $delkazavodu
        );
        $this->db->where('idzavod', $id);
        $this->db->update('zavod', $data);
    }
    public function getZavodniciEdit($id) {
        $this->db->select('idzavodnik, jmeno, prijmeni,rokNarozeni,zeme.nazev,zeme.vlajka');
        $this->db->from('zavodnik');
        $this->db->join('zeme', 'zavodnik.zeme = zeme.idzeme', 'inner');
        $this->db->order_by('idzavodnik', 'ASC');
        $this->db->where('idzavodnik',$id);
        $data = $this->db->get()->result();
        return $data;
    }
    public function setZavodnici($jmeno, $prijmeni, $rok, $zeme, $id) {
        $array = array(
            'jmeno' => $jmeno,
            'prijmeni' => $prijmeni,
            'rokNarozeni' => $rok,
            'zeme' => $zeme
        );
        $this->db->where('idzavodnik',$id);
        $this->db->update('zavodnik', $array);
        
    }
    
}
